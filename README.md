# Mobile Browser

Emulate mobile browsers

## UI
![alt text](./assets/sample.png "UI")

### Development
- Start in development mode
  ```
  yarn dev
  ```
- Build the app
  ```
  yarn build
  ```

### Libraries Used

- [Nextron](https://github.com/saltyshiomix/nextron) - Development & build
- [Chakra UI](https://chakra-ui.com/) - UI components
- [Electron Store](https://github.com/sindresorhus/electron-store) - Persist settings
- [@hadeeb/reactive](https://github.com/hadeeb/reactive) - State management
