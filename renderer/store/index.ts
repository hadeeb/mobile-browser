import { createStore, useStore } from "@hadeeb/reactive";
import { GetStoreType } from "@hadeeb/reactive/dist/internaltypes";
import ElectronStore from "electron-store";

export type MiniBrowser = {
  userAgent: string;
  devtoolOpen: boolean;
  name: string;
  width: number;
  height: number;
  zoom: number;
};

const isBrowser = typeof window !== "undefined";

const electronStore = isBrowser
  ? new ElectronStore({
      defaults: {
        url: "https://google.com",
        devices: [
          {
            name: "Galaxy S5",
            userAgent:
              "Mozilla/5.0 (Linux; Android 6.0.1; SM-G900R4 Build/MMB29M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/55.0.2883.91 Mobile Safari/537.36",
            width: 360,
            height: 640,
            devtoolOpen: false,
            zoom: 0.7
          },
          {
            name: "Iphone 6",
            userAgent:
              "Mozilla/5.0 (iPhone; CPU iPhone OS 8_0_2 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12A366 Safari/600.1.4",
            width: 375,
            height: 667,
            devtoolOpen: false,
            zoom: 0.7
          }
        ] as MiniBrowser[]
      }
    })
  : null;

const store = createStore(
  {
    ADD_WINDOW({ state }, preferences) {
      state.windows.push(preferences);
    },
    REMOVE_WINDOW({ state }, window: MiniBrowser) {
      const windows = state.windows;
      windows.splice(windows.indexOf(window), 1);
    },
    SET_URL({ state }, url) {
      state.url = url;
    },
    ZOOMIN(_, window: MiniBrowser) {
      window.zoom += 0.1;
    },
    ZOOMOUT(_, window: MiniBrowser) {
      window.zoom -= 0.1;
    },
    SWITCH_FOCUS({ state }) {
      state.focus = !state.focus;
    }
  },
  {
    url: isBrowser ? electronStore!.get("url") : "",
    windows: isBrowser ? electronStore!.get("devices") : [],
    focus: false as boolean
  }
);

function debounce(fn: () => void, delay: number): () => void {
  let id: NodeJS.Timeout;
  return function() {
    clearTimeout(id);
    id = setTimeout(fn, delay);
  };
}

const debouncedSave = debounce(function() {
  electronStore!.set("url", store.getState().url);
  electronStore!.set("devices", store.getState().windows as MiniBrowser[]);
}, 1000);

const old$ = store.$;
store.$ = function(store, action, payload) {
  old$(store, action, payload);
  debouncedSave();
};

export type StoreType = typeof store;

function useSelector<T>(selector: (store: GetStoreType<StoreType>) => T): T {
  const [store] = useStore();
  return selector(store);
}

function useDispatch() {
  return useStore<StoreType>()[1];
}

export { store, useSelector, useDispatch };
