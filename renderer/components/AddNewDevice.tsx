import React from "react";
import { useDispatch, MiniBrowser, useSelector } from "../store";
import { Button } from "@chakra-ui/core";

const presets: MiniBrowser[] = [
  {
    name: "Iphone 6 plus",
    userAgent:
      "Mozilla/5.0 (iPhone; CPU iPhone OS 8_0 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12A366 Safari/600.1.4",
    width: 414,
    height: 736,
    devtoolOpen: false,
    zoom: 0.6
  },
  {
    name: "Iphone 6",
    userAgent:
      "Mozilla/5.0 (iPhone; CPU iPhone OS 8_0_2 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12A366 Safari/600.1.4",
    width: 375,
    height: 667,
    devtoolOpen: false,
    zoom: 0.7
  },
  {
    name: "Galaxy S5",
    userAgent:
      "Mozilla/5.0 (Linux; Android 6.0.1; SM-G900R4 Build/MMB29M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/55.0.2883.91 Mobile Safari/537.36",
    width: 360,
    height: 640,
    devtoolOpen: false,
    zoom: 0.7
  }
];

function AddNewDevice() {
  const [currentDevice, setDevice] = React.useState(0);
  const dispatch = useDispatch();
  const isFocusMode = useSelector(store => store.focus);
  const onAdd = () => {
    dispatch("ADD_WINDOW", presets[currentDevice]);
  };
  return (
    <div style={{ display: isFocusMode ? "none" : "" }}>
      <select value={currentDevice} onChange={e => setDevice(+e.target.value)}>
        {presets.map((preset, i) => (
          <option key={i} value={i}>
            {preset.name}
          </option>
        ))}
      </select>
      <Button onClick={onAdd}>Add Device</Button>
    </div>
  );
}

export default AddNewDevice;
