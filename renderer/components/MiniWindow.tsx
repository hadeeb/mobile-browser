/*@jsx jsx */
import { jsx, css } from "@emotion/core";
import React, { useRef, forwardRef } from "react";
import styled from "@emotion/styled";
import {
  Box,
  Button,
  Text,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  Flex,
  IconButton,
  Stack
} from "@chakra-ui/core";
import { WebviewTag } from "electron";
import { MiniBrowser, useSelector, useDispatch } from "../store";
import { observe } from "@hadeeb/reactive";

const PhoneContainer = styled.div`
  padding: 55px 3%;
  position: relative;
  display: inline-flex;
  border-radius: 30px;
  border: 0.5px solid #e0e0e0;
  background-color: black;
  box-shadow: inset 0px 0px 0px -4px rgba(255, 255, 255, 0.1),
    1px 1px 6px rgba(0, 0, 0, 0.05), 1px 1px 8px rgba(0, 0, 0, 0.07);
  :before {
    content: "";
    display: block;
    width: 60px;
    height: 5px;
    position: absolute;
    top: 30px;
    left: 50%;
    transform: translate(-50%, -50%);
    background: #333;
    border-radius: 10px;
  }

  :after {
    content: "";
    display: block;
    width: 35px;
    height: 35px;
    position: absolute;
    left: 50%;
    bottom: -8px;
    transform: translate(-50%, -50%);
    background: #333;
    border-radius: 50%;
  }
`;

const MenuIcon: typeof IconButton = forwardRef((props, ref) => (
  <IconButton ref={ref} icon="settings" {...props} />
));

function MiniWindow({ window }: { window: MiniBrowser }) {
  const url = useSelector(store => store.url);
  const isFocusMode = useSelector(store => store.focus);
  const dispatch = useDispatch();
  const webviewRef = useRef<WebviewTag>(null);

  return (
    <Flex direction="column" m={2} textAlign="center">
      <Text display={isFocusMode ? "none" : ""} m={2}>
        {window.name}
      </Text>
      <Box display={isFocusMode ? "none" : ""}>
        <Button
          m={1}
          p={1}
          onClick={() => {
            dispatch("ZOOMOUT", window);
          }}
        >
          -
        </Button>
        <span>{Math.round(window.zoom * 100)}%</span>
        <Button
          m={1}
          p={1}
          onClick={() => {
            dispatch("ZOOMIN", window);
          }}
        >
          +
        </Button>
      </Box>
      <Flex
        display={isFocusMode ? "none" : "flex"}
        justify="space-between"
        mx={4}
        position="relative"
      >
        <Stack direction="row">
          <IconButton
            aria-label="back"
            icon="arrow-back"
            onClick={() => {
              const ref = webviewRef.current as WebviewTag;
              if (ref) {
                ref.goBack();
              }
            }}
          />
          <IconButton
            aria-label="forward"
            icon="arrow-forward"
            onClick={() => {
              const ref = webviewRef.current as WebviewTag;
              if (ref) {
                ref.goForward();
              }
            }}
          />
          <IconButton
            aria-label="reload"
            //@ts-ignore
            icon="repeat"
            onClick={() => {
              const ref = webviewRef.current as WebviewTag;
              if (ref) {
                ref.reload();
              }
            }}
          />
        </Stack>
        <Menu
          css={css`
            position: relative;
          `}
        >
          <MenuButton as={MenuIcon} />
          <MenuList>
            <MenuItem
              onClick={() => {
                const ref = webviewRef.current as WebviewTag;
                if (ref) {
                  ref.getWebContents().toggleDevTools();
                }
              }}
            >
              Toggle DevTools
            </MenuItem>
            <MenuItem
              onClick={() => {
                dispatch("REMOVE_WINDOW", window);
              }}
            >
              Remove
            </MenuItem>
          </MenuList>
        </Menu>
      </Flex>

      <PhoneContainer>
        <webview
          ref={webviewRef}
          css={css`
            background-color: #fff;
          `}
          style={{
            width: window.width * window.zoom,
            height: window.height * window.zoom
          }}
          src={url}
          useragent={window.userAgent}
          webpreferences={`zoomFactor=${window.zoom}`}
        />
      </PhoneContainer>
    </Flex>
  );
}

export default observe(MiniWindow);
