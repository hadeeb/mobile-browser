/*@jsx jsx */
import { jsx, css } from "@emotion/core";
import React, { RefObject } from "react";
import { observe } from "@hadeeb/reactive";
import { useDispatch, useSelector } from "../store";
import { Input, IconButton, Flex } from "@chakra-ui/core";
import AddNewDevice from "../components/AddNewDevice";
import MiniWindow from "../components/MiniWindow";

function Windows() {
  const windows = useSelector(store => store.windows);
  const url = useSelector(store => store.url);
  const isFocusMode = useSelector(store => store.focus);
  const dispatch = useDispatch();
  const rootRef = React.useRef<HTMLDivElement>(null);
  const ref = React.useRef<HTMLInputElement>();
  const setURL = (e: React.FormEvent) => {
    e.preventDefault();
    dispatch("SET_URL", ref.current!.value);
  };

  return (
    <>
      <form
        css={css`
          display: ${isFocusMode ? "none" : "flex"};
        `}
        onSubmit={setURL}
      >
        <Input
          ref={ref as RefObject<HTMLInputElement>}
          defaultValue={url}
        ></Input>
      </form>
      <AddNewDevice />
      <IconButton
        aria-label="toggle focus mode"
        icon={isFocusMode ? "unlock" : "lock"}
        onClick={() => {
          dispatch("SWITCH_FOCUS");
        }}
        title={isFocusMode ? "Show All" : "Hide All"}
        marginLeft="auto"
      />

      <Flex wrap="wrap" ref={rootRef}>
        {windows.map((window, index) => (
          <MiniWindow key={index} window={window} />
        ))}
      </Flex>
    </>
  );
}

export default observe(Windows);
