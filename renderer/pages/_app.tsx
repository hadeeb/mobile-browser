import React from "react";
import App from "next/app";
import { ThemeProvider, CSSReset } from "@chakra-ui/core";
import { StoreProvider } from "@hadeeb/reactive";
import { store } from "../store";
import { Global } from "@emotion/core";

class MyApp extends App {
  render() {
    const { Component, pageProps } = this.props;
    return (
      <StoreProvider store={store}>
        <Global
          styles={`
            #__next {
              display: flex;
              flex-direction: column;
            }
          `}
        ></Global>
        <ThemeProvider>
          <CSSReset />
          <Component {...pageProps} />
        </ThemeProvider>
      </StoreProvider>
    );
  }
}
export default MyApp;
